import { useEffect } from "react";
import styles from "./App.module.scss";
import { useState } from "react";

function App() {
  const [isModalOpen, setisModalOpen] = useState(false);

  useEffect(() => {
    if (isModalOpen) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "unset";
    }
  }, [isModalOpen]);

  const handleAddProduct = () => {
    setisModalOpen(true);
  };

  const handleCloseModal = () => {
    setisModalOpen(false);
  };
  return (
    <>
      <div className={styles["layout"]}>
        <div className={styles["header"]}>
          <div className={styles["back-button"]}>
            <img alt="" src={`/assets/icons/arrow-left-icon.svg`} />
            <div>Back</div>
          </div>
          <div className={styles["title"]}>
            <img alt="" src={`/assets/icons/box-icon.svg`} />
            <h6>Product list</h6>
          </div>
        </div>
        <div className={styles["container"]}>
          <div className={styles["utility"]}>
            <div className={styles["search"]}>
              <input type="text" placeholder="Search product" />
              <img alt="" src={`/assets/icons/search-icon.svg`} />
            </div>
            <div className={styles["import"]}>
              <img alt="" src={`/assets/icons/import-icon.svg`} />
              <div>Import</div>
            </div>
          </div>
          <div className={styles["category"]}>
            <div className={styles["category-title"]}>Category</div>
            <div className={styles["category-lists"]}>
              <div className={styles["list"]}>Cocktail</div>
              <div className={styles["list"]}>Shooters</div>
              <div className={styles["list"]}>Premium Spirits</div>
              <div className={styles["list"]}>Non-Alcoholic Beverages</div>
            </div>
            <div className={styles["icon"]}>
              <img alt="" src={`/assets/icons/pencil-icon.svg`} />
            </div>
          </div>
          <div className={styles["product-lists"]}>
            <div className={styles["item"]}>
              <div className={styles["price"]}>15.000 NT</div>
              <div className={styles["thumbnail"]}>
                <img alt="" src={`/assets/img/example-img.png`} />
              </div>
              <div className={styles["item-title"]}>
                Pack of Beer (6pcs of heineken)
              </div>
              <div className={styles["item-desc"]}>
                Heineken lager beer, or known as just Heineken, is one of the
                pale beers with 5% alcohol.
              </div>
              <div className={styles["stock"]}>6pcs</div>
            </div>
            <div className={styles["item"]} onClick={handleAddProduct}>
              <div className={styles["add"]}>
                <img alt=""
                  className={styles["add-icon"]}
                  src={`/assets/icons/plus-icon.svg`}
                />
                <div>Add New Product</div>
              </div>
            </div>
          </div>
        </div>
        <div className={styles["footer"]}>
          <div className={`${styles["step"]} ${styles["active"]}`}>
            Nightclub Profile
          </div>
          <div className={`${styles["step"]} ${styles["active"]}`}>
            Featured
          </div>
          <div className={`${styles["step"]} ${styles["active"]}`}>
            Beverages
          </div>
          <div className={`${styles["step"]}`}>Table Information</div>
          <div className={`${styles["step"]}`}>Operational Hour</div>
          <div className={`${styles["step"]} ${styles["next"]}`}>NEXT</div>
        </div>
      </div>
      {isModalOpen && (
        <div className={styles["modal-layout"]}>
          <div className={styles["modal-bg"]} />
          <div className={styles["modal-container"]}>
            <div className={styles["modal-form"]}>
              <div className={styles["form-close"]} onClick={handleCloseModal}>
                <img alt="" src={`/assets/icons/cross-icon.svg`} />
              </div>
              <div className={styles["form-title"]}>Add menu</div>
              <div className={styles["form"]}>
                <div className={styles["form-row"]}>
                  <div className={styles["input-wrapper"]}>
                    <label>Your product name</label>
                    <div className={styles["input-box"]}>
                      <input type="text" placeholder="Product name" />
                    </div>
                  </div>
                  <div className={styles["input-wrapper"]}>
                    <label>Menu name</label>
                    <div className={styles["input-box"]}>
                      <input type="text" placeholder="menu code" />
                    </div>
                  </div>
                </div>
                <div className={styles["form-row"]}>
                  <div className={styles["input-wrapper"]}>
                    <label>Category</label>
                    <div className={`${styles["input-box"]} ${styles["full"]}`}>
                      <select>
                        <option value="" disabled selected>
                          Select category
                        </option>
                        <option value="hurr">Durr</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div className={styles["form-row"]}>
                  <div className={styles["input-wrapper"]}>
                    <label>
                      Tell me more about your product<span>*</span>
                    </label>
                    <div className={styles["input-box"]}>
                      <textarea placeholder="Product description" />
                    </div>
                  </div>
                </div>
                <div className={styles["form-row"]}>
                  <div className={styles["input-wrapper"]}>
                    <label>
                      Price<span>*</span>
                    </label>
                    <div className={styles["input-box"]}>
                      <div>NT$</div>
                      <input type="text" placeholder="Price" />
                    </div>
                  </div>
                  <div className={styles["input-wrapper"]}>
                    <label>Discount price (optional)</label>
                    <div className={styles["input-box"]}>
                      <div>NT$</div>
                      <input type="text" placeholder="Discounted price" />
                    </div>
                  </div>
                </div>
                <div className={styles["form-row"]}>
                  <div className={styles["input-wrapper"]}>
                    <label>
                      Image<span>*</span>
                    </label>
                    <div className={`${styles["input-box"]} ${styles["full"]}`}>
                      <label
                        for="file-upload"
                        className={styles["label-upload"]}
                      >
                        <div>Select File</div>
                        <img alt="" src={`/assets/icons/import-purple-icon.svg`} />
                      </label>
                      <input
                        id="file-upload"
                        type="file"
                        placeholder="Select File"
                      />
                    </div>
                  </div>
                </div>
                <div className={`${styles["form-row"]} ${styles["between"]}`}>
                  <div className={styles["toggle-title"]}>Enable Variant</div>
                  <div className={styles["toggle"]}>
                    <input type="checkbox" id="switch" />
                    <label for="switch">Toggle</label>
                  </div>
                </div>
                <div className={styles["form-row"]}>
                  <div className={styles["form-button"]}>Add product</div>
                </div>
              </div>
            </div>
            <div className={styles["modal-tips"]}>
              <div className={styles["tips-title"]}>Looking for variant ?</div>
              <div className={styles["tips-content"]}>
                Don’t worry!!
                <br />
                <br />
                size, sugar level, ice level will be unlocked if you already
                approved by admin.
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default App;
